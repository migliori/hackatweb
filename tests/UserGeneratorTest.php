<?php

namespace App\Tests;

use App\Entity\Hackathon;
use App\Entity\Membre;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserGeneratorTest extends KernelTestCase
{

    public function testSomething()
    {
        $this->member = new Membre();
        $this->member->setLastname("Jean");

        $this->assertEquals('Jean', $this->member->getLastname());
    }

    public function testOtherThing() {
        $this->hackathon = new Hackathon();
        $this->hackathon->setName("test");

        $this->assertEquals('test', $this->hackathon->getName());

    }
}