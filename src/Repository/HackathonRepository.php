<?php

namespace App\Repository;

use App\Entity\Hackathon;
use App\Entity\Organisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
/**
 * @method Hackathon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hackathon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hackathon[]    findAll()
 * @method Hackathon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HackathonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hackathon::class);
    }

    /**
     * Retourne la liste des hackathons qui sont finis
     * @return Hackathon[]
     */
    public function findAllHackathonFinis()
    {
        $date = new \DateTime('now');
        return $this->createQueryBuilder('h')
            ->andWhere("h.date < :todayDate")
            ->setParameter("todayDate", $date->format('Y-m-d'))
            ->orderBy("h.date")
            ->getQuery()
            ->getResult();
    }

    /**
     * Retourne la liste des hackathons qui sont en cours
     * @return Hackathon[]
     */
    public function finAllHackathonEnCours(): array
    {
        $date = new \DateTime('now');
        return $this->createQueryBuilder('h')
            ->andWhere("h.date > :todayDate and h.caninscription = 0")
            ->setParameter("todayDate", $date->format('Y-m-d'))
            ->orderBy("h.date")
            ->getQuery()
            ->getResult();
    }

    /**
     * Retourne la liste des hackathons qui vont se dérouler prochainement
     * @return Hackathon[]
     */
    public function findAllHackathonsFutur(): array
    {
        $date = new \DateTime('now');
        return $this->createQueryBuilder('h')
            ->andWhere("h.date > :todayDate and h.caninscription = 1")
            ->setParameter("todayDate", $date->format('Y-m-d'))
            ->orderBy("h.date")
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Hackathon[] Returns an array of Hackathon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Hackathon
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
