<?php

namespace App\Repository;

use App\Entity\Membre;
use App\Entity\Participant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Participant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Participant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Participant[]    findAll()
 * @method Participant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParticipantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Participant::class);
    }

    /**
     * Pour un idMembre donnée, retourne le participant si le membre est présent dans la table participant, null sinon
     * @param int $idMembre
     * @return Participant|null
     */
    public function findByIdMember(int $idMembre): ?Participant{
        return $this->createQueryBuilder('p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $idMembre, ParameterType::INTEGER)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Ajoute à la table participant l'id du membre passé en paramètre
     * @param int $idMembre
     * @return void
     */
    public function InsertMembre(int $idMembre){
        $connexion = $this->getEntityManager()->getConnection();
        $sql = 'Insert into participant (id) values (?)';
        $stmt = $connexion->prepare($sql);
        $stmt->executeQuery([$idMembre]);
    }

    /**
     * Supprimer de la table participant l'id du membre passé en paramètre
     * @param int $idMembre
     * @return void
     */
    public function DeleteMembre(int $idMembre){
        $connexion = $this->getEntityManager()->getConnection();
        $sql = 'delete from participant where id = ?';
        $stmt = $connexion->prepare($sql);
        $stmt->executeQuery([$idMembre]);
    }

    // /**
    //  * @return Participant[] Returns an array of Participant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Participant
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
