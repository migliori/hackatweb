<?php

namespace App\Repository;

use App\Entity\Membre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Membre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Membre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Membre[]    findAll()
 * @method Membre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MembreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Membre::class);
    }

    /**
     * Met à jour les informations du membre ($id) passé en paramètre
     * On utilise une procédure stockée (updatemembreuser), permettant de mettre à jour la table membre ainsi que la table user
     * @param int $id
     * @param string $firstName
     * @param string $lastName
     * @param string $phone
     * @param string $email
     * @param string $password
     */
    public function updateMemberUser(int $id, string $firstName, string $lastName, string $phone, string $email, string $password)
    {
        $connexion = $this->getEntityManager()->getConnection();
        $sql = 'select updatemembreuser(?, ?, ?, ?, ?, ?)';
        $stmt = $connexion->prepare($sql);
        $stmt->executeQuery([$id, $lastName, $firstName, $phone, $email, $password]);
    }

    /**
     * Crée un membre et son compte utilisateur.
     * On utilise une procédure stockée (createmembreuser), permettant de créer le membre et l'utilisateur associé.
     * @param string $firstName
     * @param string $lastName
     * @param string $phone
     * @param string $email
     * @param string $password
     */
    public function newMemberUser(string $firstName, string $lastName, string $phone, string $email, string $password)
    {
        $connexion = $this->getEntityManager()->getConnection();
        $sql = 'select createmembreuser(?, ?, ?, ?, ?)';
        $stmt = $connexion->prepare($sql);
        $stmt->executeQuery([$lastName, $firstName, $phone, $email, $password]);
    }

    // /**
    //  * @return Members[] Returns an array of Members objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Members
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
