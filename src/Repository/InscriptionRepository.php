<?php

namespace App\Repository;

use App\Entity\Inscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Inscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Inscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Inscription[]    findAll()
 * @method Inscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inscription::class);
    }

    /**
     * Pour l'id du membre inscrit passé en paramètre, passe à 1 (true) le champ isValid.
     * Le champ isValid est un boolean indiquant si l'inscription est valide ou non.
     * @param int $idInscrit
     * @return void
     */
    public function setIsValid(int $idInscrit) : void
    {
        $connexion = $this->getEntityManager()->getConnection();
        $sql = 'UPDATE inscription SET isvalid = 1 WHERE id = ?';
        $stmt = $connexion->prepare($sql);
        $stmt->executeQuery([$idInscrit]);
    }

    /**
     * Pour l'id de l'hackathon passé en paramètre, passe à 0 (false) le champ isValid, de tous les membres inscrits.
     * @param int $idHackathon
     * @return void
     */
    public function removeAll(int $idHackathon) : void
    {
        $connexion = $this->getEntityManager()->getConnection();
        $sql = 'UPDATE inscription SET isvalid = 0 WHERE idhackathon = ?';
        $stmt = $connexion->prepare($sql);
        $stmt->executeQuery([$idHackathon]);
    }

    /**
     * Pour un hackathon et un membre donné, retourne le membre inscrit s'il existe, sinon retourne faux.
     * @param int $idHackathon
     * @param int $idMembre
     * @return bool|Inscription
     */
    public function findByUserByHackathon(int $idHackathon, int $idMembre)
    {
        $res = false;

        $req =  $this->createQueryBuilder('i')
                     ->andWhere('i.idhackathon = :idhackathon')
                     ->andWhere('i.membre = :idmembre')
                     ->setParameter('idhackathon',$idHackathon, ParameterType::INTEGER)
                     ->setParameter('idmembre',$idMembre, ParameterType::INTEGER)
                     ->getQuery()
                     ->execute();

        if(!empty($req)) {
            $res = $req[0];
        }

        return $res;
    }

    /**
     * Pour un membre donné, retourne null s'il n'est pas présent dans la table inscription, sinon retourne ses inscriptions.
     * @param int $idMembre
     * @return Inscription[]|null
     */
    public function findByUser(int $idMembre) : ?array
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.membre = :idmembre')
            ->setParameter('idmembre',$idMembre, ParameterType::INTEGER)
            ->getQuery()
            ->execute();
    }

    // /**
    //  * @return Inscription[] Returns an array of Inscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


//    public function findOneBySomeField($value): ?Inscription
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
