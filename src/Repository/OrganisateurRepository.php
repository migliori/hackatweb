<?php

namespace App\Repository;

use App\Entity\Organisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\ParameterType;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Organisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Organisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Organisateur[]    findAll()
 * @method Organisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganisateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Organisateur::class);
    }

    /**
     * pour un hackathon et un membre donné, retourne vrai si le membre est organisateur sur l'hackathon, faux sinon.
     * @param int $idMembre
     * @return bool
     */
    public function isOrganisateur(int $idMembre, int $idHackathon)
    {
        $bool = false;

        $req = $this->createQueryBuilder('o')
            ->innerJoin('o.hackathons', 'h')
            ->andWhere('o.id = :idMembre')
            ->andWhere('h.id = :idHackathon')
            ->setParameter(':idMembre',$idMembre,ParameterType::INTEGER)
            ->setParameter(':idHackathon', $idHackathon, ParameterType::INTEGER)
            ->getQuery()
            ->execute();

        if(!empty($req)) {
            $bool = true;
        }

        return $bool;
    }

    // /**
    //  * @return Organisateur[] Returns an array of Organisateur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Organisateur
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


}
