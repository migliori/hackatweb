<?php

namespace App\Repository;

use App\Entity\TypeInscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeInscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeInscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeInscription[]    findAll()
 * @method TypeInscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeInscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeInscription::class);
    }

    // /**
    //  * @return TypeInscription[] Returns an array of TypeInscription objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeInscription
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
