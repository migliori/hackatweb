<?php

namespace App\Entity;

use App\Repository\AnimateurRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Animateur
 * @ORM\Entity(repositoryClass=AnimateurRepository::class)
 * @ORM\Table(name="animateur")
 */
class Animateur extends Membre
{

}
