<?php

namespace App\Entity;

use App\Repository\ExpertRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Expert
 * @ORM\Entity(repositoryClass=ExpertRepository::class)
 * @ORM\Table(name="expert")
 */
class Expert extends Membre
{

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $job;

    public function getJob(): ?string
    {
        return $this->job;
    }

    public function setJob(string $job): self
    {
        $this->job = $job;

        return $this;
    }
}
