<?php

namespace App\Entity;

use App\Repository\MembreRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="membre")
 * @ORM\Entity(repositoryClass=MembreRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "membre" = "Membre",
 *     "participant" = "Participant",
 *     "expert" = "Expert",
 *     "jury" = "Jury",
 *     "organisateur" = "Organisateur",
 *     "animateur" = "Animateur"
 * })
 */
class Membre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $lastname;

    /**
     * @ORM\Column(type="string")
     */
    private $firstname;

    /**
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="membre")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setMembre(null);
        }

        // set the owning side of the relation if necessary
        if ($user !== null && $user->getMembre() !== $this) {
            $user->setMembre($this);
        }

        $this->user = $user;

        return $this;
    }

    public function __toString()
    {
        return $this->firstname;
    }


}
