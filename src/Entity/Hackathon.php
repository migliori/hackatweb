<?php

namespace App\Entity;

use App\Repository\HackathonRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HackathonRepository::class)
 */
class Hackathon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $target;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $topic;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbentrant;

    /**
     * @ORM\ManyToOne(targetEntity="Organisateur", inversedBy="hackathons")
     * @ORM\JoinColumn(name="idOrganisateur")
     */
    private $organisateur;

    /**
     * @ORM\Column(type="integer")
     */
    private $idtypeinscription;

    /**
     * @ORM\Column(type="smallint")
     */
    private $canvote;

    /**
     * @ORM\Column(type="smallint")
     */
    private $caninscription;

    /**
     * @ORM\Column(type="smallint")
     */
    private $canposition;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getTarget(): ?string
    {
        return $this->target;
    }

    public function setTarget(string $target): self
    {
        $this->target = $target;

        return $this;
    }

    public function getTopic(): ?string
    {
        return $this->topic;
    }

    public function setTopic(string $topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNbentrant(): ?int
    {
        return $this->nbentrant;
    }

    public function setNbentrant(int $nbentrant): self
    {
        $this->nbentrant = $nbentrant;

        return $this;
    }

    public function getIdtypeinscription(): ?int
    {
        return $this->idtypeinscription;
    }

    public function setIdtypeinscription(int $idtypeinscription): self
    {
        $this->idtypeinscription = $idtypeinscription;

        return $this;
    }

    public function getCanvote(): ?int
    {
        return $this->canvote;
    }

    public function setCanvote(int $canvote): self
    {
        $this->canvote = $canvote;

        return $this;
    }

    public function getCaninscription(): ?int
    {
        return $this->caninscription;
    }

    public function setCaninscription(int $caninscription): self
    {
        $this->caninscription = $caninscription;

        return $this;
    }

    public function getCanposition(): ?int
    {
        return $this->canposition;
    }

    public function setCanposition(int $canposition): self
    {
        $this->canposition = $canposition;

        return $this;
    }

    public function getOrganisateur(): ?Organisateur
    {
        return $this->organisateur;
    }

    public function setOrganisateur(?Organisateur $organisateur): self
    {
        $this->organisateur = $organisateur;

        return $this;
    }
}
