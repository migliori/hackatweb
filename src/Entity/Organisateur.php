<?php

namespace App\Entity;

use App\Repository\OrganisateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Organisateur
 * @ORM\Entity(repositoryClass=OrganisateurRepository::class)
 * @ORM\Table(name="organisateur")
 */
class Organisateur extends Membre
{
    /**
     * @ORM\OneToMany(targetEntity="Hackathon", mappedBy="organisateur")
     */
    private $hackathons;

    public function __construct()
    {
        $this->hackathons = new ArrayCollection();
    }

    /**
     * @return Collection|Hackathon[]
     */
    public function getHackathons(): Collection
    {
        return $this->hackathons;
    }

    public function addHackathon(Hackathon $hackathon): self
    {
        if (!$this->hackathons->contains($hackathon)) {
            $this->hackathons[] = $hackathon;
            $hackathon->setOrganisateur($this);
        }

        return $this;
    }

    public function removeHackathon(Hackathon $hackathon): self
    {
        if ($this->hackathons->removeElement($hackathon)) {
            // set the owning side to null (unless already changed)
            if ($hackathon->getOrganisateur() === $this) {
                $hackathon->setOrganisateur(null);
            }
        }

        return $this;
    }
}
