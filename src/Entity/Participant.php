<?php

namespace App\Entity;

use App\Repository\ParticipantRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Participant
 * @ORM\Entity(repositoryClass=ParticipantRepository::class)
 * @ORM\Table(name="participant")
 */
class Participant extends Membre
{

    public function __toString()
    {
        return parent::getFirstname();
    }

}
