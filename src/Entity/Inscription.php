<?php

namespace App\Entity;

use App\Repository\InscriptionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InscriptionRepository::class)
 */
class Inscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $dateinscription;

    /**
     * @ORM\Column(type="smallint")
     */
    private $isvalid;

    /**
     * @ORM\OneToOne(targetEntity="Participant", mappedBy="id")
     * @ORM\JoinColumn(name="idmembre", referencedColumnName="id")
     */
    private $membre;

    /**
     * @ORM\Column(type="integer")
     */
    private $idhackathon;

    /**
     * @ORM\Column(type="integer")
     */
    private $idprojet;

    /**
     * @ORM\Column(type="integer")
     */
    private $idequipe;

    /**
     * @param $dateinscription
     * @param $isvalid
     * @param $membre
     * @param $idhackathon
     * @param $idprojet
     * @param $idequipe
     */

    public function __construct($dateinscription, $isvalid, $membre, $idhackathon, $idprojet, $idequipe)
    {
        $this->dateinscription = $dateinscription;
        $this->isvalid = $isvalid;
        $this->membre = $membre;
        $this->idhackathon = $idhackathon;
        $this->idprojet = $idprojet;
        $this->idequipe = $idequipe;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateinscription(): ?\DateTimeInterface
    {
        return $this->dateinscription;
    }

    public function setDateinscription(\DateTimeInterface $dateinscription): self
    {
        $this->dateinscription = $dateinscription;

        return $this;
    }

    public function getIsvalid(): ?int
    {
        return $this->isvalid;
    }

    public function setIsvalid(int $isvalid): self
    {
        $this->isvalid = $isvalid;

        return $this;
    }

    public function getMembre(): ?Membre
    {
        return $this->membre;
    }

    public function setMembre(?Membre $membre): self
    {
        $this->membre = $membre;

        return $this;
    }

    public function getIdhackathon(): ?int
    {
        return $this->idhackathon;
    }

    public function setIdhackathon(int $idhackathon): self
    {
        $this->idhackathon = $idhackathon;

        return $this;
    }

    public function getIdprojet(): ?int
    {
        return $this->idprojet;
    }

    public function setIdprojet(int $idprojet): self
    {
        $this->idprojet = $idprojet;

        return $this;
    }

    public function getIdequipe(): ?int
    {
        return $this->idequipe;
    }

    public function setIdequipe(int $idequipe): self
    {
        $this->idequipe = $idequipe;

        return $this;
    }
}
