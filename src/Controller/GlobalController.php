<?php

namespace App\Controller;

use App\Entity\Hackathon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class GlobalController extends AbstractController
{
    /**
     * Page d'accueil
     * Route associée: index
     * Path: /
     */
    public function index(): Response
    {
        $lesHackathonsFinis = $this->getDoctrine()->getRepository(Hackathon::class)->findAllHackathonFinis();
        $lesHackathonsEnCours = $this->getDoctrine()->getRepository(Hackathon::class)->finAllHackathonEnCours();
        $lesHackathonFutur = $this->getDoctrine()->getRepository(Hackathon::class)->findAllHackathonsFutur();

        return $this->render('global/index.html.twig', [
            "lesHackathonsFinis" => $lesHackathonsFinis,
            "lesHackathonsEnCours" => $lesHackathonsEnCours,
            "lesHackathonFutur" => $lesHackathonFutur
        ]);
    }
}
