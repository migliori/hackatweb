<?php

namespace App\Controller;

use App\Entity\Membre;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * permet l'authentification de l'utilisateur
     * Route: app_login
     * Path: /login
     * @param AuthenticationUtils $authenticationUtils
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * permet la déconnexion d'un utilisateur
     * Route: app_logout
     * Path: /logout
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * permet la modification d'un compte utilisateur
     * Route associée : my_account
     * Path: /account
     * Method: GET
     */
    public function modifPage(): Response
    {
        return $this->render("security/account.html.twig", [
            "message" => null
        ]);
    }

    /**
     * Fonction appelée lors de la modification des informations de l'utilisateur.
     * Route associée : my_account_change
     * Path: /account
     * Method: POST
     */
    public function modifInfo(): Response
    {
        $redirect = "my_account";

        if(isset($_POST["id"]) && isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["phone"]) && isset($_POST["email"]) && isset($_POST["password"]))
        {
            if(!empty(trim($_POST['firstName'])) && !empty(trim($_POST['lastName'])) && !empty(trim($_POST['email'])) && !empty(trim($_POST['password'])))
            {
                $user =  $this->getDoctrine()->getRepository(User::class)->find($_POST["id"]);
                $this->getDoctrine()->getRepository(Membre::class)->updateMemberUser(trim($_POST["id"]), trim($_POST["firstName"]), trim($_POST["lastName"]), trim($_POST["phone"]), trim($_POST["email"]), trim($_POST["password"]) == "" ? $user->getPassword() : password_hash($_POST["password"],PASSWORD_ARGON2I));

                $redirect =  "index";
            }
        }
        return $this->redirectToRoute($redirect);
    }

    /**
     * Page proposant un formulaire de création de compte.
     * Si un utilisateur connecté essaie d'accéder cette page, le redirige vers les informations de son compte.
     * Route associée : app_newAccount
     * Path: /newaccount
     * Method: GET
     */
    public function newAccountPage(): Response
    {
        $redirect = "security/newAccount.html.twig";

        if($this->getUser())
        {
            $redirect = "security/account.html.twig";
        }
        return $this->render($redirect);
    }

    /**
     * Enregistre les informations dans la base de données à la soumission du formulaire de création de compte
     * Route associée : app_newAccount_submit
     * Path: /newaccount
     * Method: POST
     */
    public function newAccountInfo(): Response
    {
        $redirect = "app_newAccount";

        if(isset($_POST["FirstName"]) && isset($_POST["LastName"]) && isset($_POST["Phone"]) && isset($_POST["Mail"]) && isset($_POST["password"]))
        {
            if(!empty(trim($_POST['FirstName'])) && !empty(trim($_POST['LastName'])) && !empty(trim($_POST['Mail'])) && !empty(trim($_POST['password'])))
            {
                if($this->getDoctrine()->getRepository(User::class)->mailAlreadyExist(trim($_POST["Mail"])) == null)
                {
                    $this->getDoctrine()->getRepository(Membre::class)->newMemberUser(trim($_POST["FirstName"]), trim($_POST["LastName"]), trim($_POST["Phone"]), trim($_POST["Mail"]), trim( password_hash($_POST["password"],PASSWORD_ARGON2I)));
                }
                $redirect = "index";
            }

        }
        return $this->redirectToRoute($redirect);
    }
}
