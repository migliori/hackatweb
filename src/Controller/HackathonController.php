<?php


namespace App\Controller;

use App\Entity\Hackathon;
use App\Entity\Inscription;
use App\Entity\Membre;
use App\Entity\Organisateur;
use App\Entity\Participant;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class HackathonController extends AbstractController
{
    /**
     * Redirige vers la page de l'hackathon passé en paramètre
     * Route associée : showHackathon
     * Path: /hackathon/{id}
     * @param $id
     */
    public function hackathonDetails($id): Response
    {
        $hackathon = $this->getDoctrine()->getRepository(Hackathon::class)->find($id);
        $lesInscrits = $this->getDoctrine()->getRepository(Inscription::class)->findBy(['idhackathon' => $id]);
        $isInscrit = false;
        $isOrganisateur = false;
        $isParticipant = false;

        if ($this->getUser() !== null)
        {
            $userInscrit = $this->getDoctrine()->getRepository(Inscription::class)->findByUserByHackathon($id,$this->getUser()->getMembre()->getId());
            $isOrganisateur = $this->getDoctrine()->getRepository(Organisateur::class)->isOrganisateur($this->getUser()->getMembre()->getId(), $hackathon->getId());
            $isParticipant = $this->getUser()->getMembre() instanceof Participant;

            if(!empty($userInscrit))
            {
                $isInscrit = true;
            }
        }

        return $this->render("hackathon/showHackathon.html.twig", [
            "hackathon" => $hackathon,
            "isInscrit" => $isInscrit,
            "isParticipant" => $isParticipant,
            "isOrganisateur" => $isOrganisateur,
            "lesInscrits" => $lesInscrits
        ]);
    }

    /**
     * La variable $_POST['idInscrits'] est une liste contenant toutes les inscriptions validées par l'organisateur
     * Cette fonction permet de passer à 1 (vrai) le champ isValid de tous les membres inscrits présent dans cette variable.
     *
     * Une fois le traitement terminée, redirige vers la page de l'hackathon sur laquelle l'organisateur se trouve.
     *
     * Route associée : add_valid_inscription
     * Path: /manageInscription
     */
    public function addValidInscription() : Response
    {
        if(!empty($_POST['idInscrits']))
        {
            $this->getDoctrine()->getRepository(Inscription::class)->removeAll($_POST['idHackathon']);

            foreach ($_POST['idInscrits'] as $idInscrit)
            {
                $this->getDoctrine()->getRepository(Inscription::class)->setIsValid($idInscrit);
            }
        }
        else
        {
            $this->getDoctrine()->getRepository(Inscription::class)->removeAll($_POST['idHackathon']);
        }
        return $this->redirectToRoute("showHackathon", [
            "id" => $_POST['idHackathon']
        ]);
    }

    /**
     * Permet d'ajouter un utilisateur à la liste des inscrits de l'hackathon donné.
     * Une fois le traitement terminé, redirige le membre inscrit sur la page de l'hackathon sur laquelle il était.
     *
     * Route associée : ajouteUserHackathon
     * Path: /hackathon/create/{idHackathon}/{idUser}
     *
     * @param int $idHackathon
     * @param int $idUser
     */
    public function addUser(int $idHackathon, int $idUser):Response
    {
        if($this->getUser() != null)
        {
            if($idUser == $this->getUser()->getMembre()->getId())
            {
                $entityManager = $this->getDoctrine()->getManager();
                $date = new \DateTime();
                $membre = $this->getDoctrine()->getRepository(Membre::class)->find($idUser);
                $inscription = new Inscription($date,0,$membre,$idHackathon,null,null);
                $verifMemberIsParticipant = $this->getDoctrine()->getRepository(Participant::class)->findByIdMember($idUser);

                if($verifMemberIsParticipant == null)
                {
                    $this->getDoctrine()->getRepository(Participant::class)->InsertMembre($idUser);
                }

                $entityManager->persist($inscription);
                $entityManager->flush($inscription);
            }
        }

        return $this->redirectToRoute("showHackathon",[
            'id' => $idHackathon
        ]);
    }

    /**
     * Supprime l'inscription de l'utilisateur donnée.
     * Si l'utilisateur n'a plus d'inscription on le supprime de la liste des participants (table participant).
     * Une fois le traitement terminé, redirige le membre désinscrit sur la page de l'hackathon sur laquelle il était.
     *
     * Route associée : deleteUserHackathon
     * Path: /hackathon/delete/{idHackathon}/{idUser}
     *
     * @param $idHackathon
     * @param $idUser
     */
    public function deleteUser($idHackathon,$idUser):Response
    {
        if($this->getUser() != null)
        {
            if($this->getUser()->getMembre()->getId() == $idUser)
            {
                $entityManager = $this->getDoctrine()->getManager();
                $userInscrit = $this->getDoctrine()->getRepository(Inscription::class)->findByUserByHackathon($idHackathon,$idUser);

                $entityManager->remove($userInscrit);
                $entityManager->flush($userInscrit);

                $lesInscriptions = $this->getDoctrine()->getRepository(Inscription::class)->findByUser($idUser);

                if(empty($lesInscriptions))
                {
                    $this->getDoctrine()->getRepository(Participant::class)->DeleteMembre($idUser);
                }
            }
        }

        return $this->redirectToRoute("showHackathon",[
            'id'=>$idHackathon
        ]);
    }
}